# No previous file for Anbennar4751
owner = Y71
controller = Y71
add_core = Y71
culture = bokai
religion = righteous_path

hre = no

base_tax = 6
base_production = 7
base_manpower = 4

trade_goods = paper

capital = ""

is_city = yes

add_permanent_province_modifier = {
	name = harimari_minority_integrated_large
	duration = -1
}